# Author-Felix Eve
# Description-Create a geodesic sphere
from typing import Dict, Any

import adsk.core, adsk.fusion, adsk.cam, traceback

from .lib.icosahedron import Icosahedron
from .lib.dodecahedron import Dodecahedron

handlers = []
_app     = adsk.core.Application.get()
_ui      = _app.userInterface
_design  = adsk.fusion.Design.cast(None)


# Event handler for the commandCreated event.
class CommandCreatedEventHandler(adsk.core.CommandCreatedEventHandler):
    def __init__(self):
        super().__init__()

    def notify(self, args):
        try:
            eventArgs = adsk.core.CommandCreatedEventArgs.cast(args)
            cmd = eventArgs.command

            # Connect to the execute event.
            onExecute = CommandExecuteHandler()
            cmd.execute.add(onExecute)
            handlers.append(onExecute)

            # Connect ot the preview event.
            onExecutePreview = ExecutePreviewHandler()
            cmd.executePreview.add(onExecutePreview)
            handlers.append(onExecutePreview)

            global _design
            _design = adsk.fusion.Design.cast(_app.activeProduct)

            if _design:
                inputs = cmd.commandInputs

                # Add inputs
                dlu = _design.unitsManager.defaultLengthUnits
                vi = adsk.core.ValueInput
                tldds = adsk.core.DropDownStyles.TextListDropDownStyle

                # Platonic solid
                platonicSolidInput = inputs.addDropDownCommandInput('platonicSolid', 'Platonic Solid', tldds)
                platonicSolids = platonicSolidInput.listItems
                platonicSolids.add('Icosahedron', True, '')
                platonicSolids.add('Dodecahedron', False, '')
                platonicSolids.add('Octahedron', False, '')
                platonicSolids.add('Cube', False, '')
                platonicSolids.add('Tetrahedron', False, '')

                # Triangulation method
                triMethodInput = inputs.addDropDownCommandInput('triangulationMethod', 'Triangulation Method', tldds)
                triangulationMethods = triMethodInput.listItems
                triangulationMethods.add('Class I', True, '')
                triangulationMethods.add('Class II', False, '')
                triangulationMethods.add('Centre point', False, '')

                # Frequency
                inputs.addIntegerSpinnerCommandInput('frequencyInput', 'Frequency', 1, 6, 1, 3)

                message = 'Frequency is capped at 3 for Class II , 4 for Centre point and 1 for Slash'
                inputs.addTextBoxCommandInput('frequency_cap', '', message, 2, True)

                # Secondary triangulation method
                secondaryTriMethodInput = inputs.addDropDownCommandInput('secondaryTriangulationMethod', 'Secondary Triangulation Method', tldds)
                secondaryTriMethods = secondaryTriMethodInput.listItems
                secondaryTriMethods.add('None', True, '')
                secondaryTriMethods.add('Class I', False, '')
                secondaryTriMethods.add('Class II', False, '')
                secondaryTriMethods.add('Centre point', False, '')

                message = 'Frequency is always 1 for secondary triangulation'
                inputs.addTextBoxCommandInput('secondary_frequency_cap', '', message, 2, True)

                # Radius
                inputs.addValueInput('radiusInput', 'Radius', dlu, vi.createByReal(5))

                # Position
                inputs.addValueInput('xValInput', 'X Position', dlu, vi.createByReal(0))
                inputs.addValueInput('yValInput', 'Y Position', dlu, vi.createByReal(0))
                inputs.addValueInput('zValInput', 'Z Position', dlu, vi.createByReal(0))

                # Sphere info
                inputs.addTextBoxCommandInput('sphereInfo', 'Sphere Info', '', 10, True)

            else:
                _ui.messageBox('You must be in a modeling related workspace.')
                return False
        except:
            _ui.messageBox('Unexpected failure:\n{}'.format(traceback.format_exc()))


# Event handler for the executePreview event.
class ExecutePreviewHandler(adsk.core.CommandEventHandler):
    def __init__(self):
        super().__init__()

    def notify(self, args):
        eventArgs = adsk.core.CommandEventArgs.cast(args)
        DrawGeodesicSphere(eventArgs.firingEvent.sender.commandInputs)


# Event handler for the execute event.
class CommandExecuteHandler(adsk.core.CommandEventHandler):
    def __init__(self):
        super().__init__()

    def notify(self, args):
        eventArgs = adsk.core.CommandEventArgs.cast(args)
        DrawGeodesicSphere(eventArgs.command.commandInputs)


def DrawGeodesicSphere(inputs):
    try:

        # Get the values from the command inputs.
        platonicSolidInput  = adsk.core.DropDownCommandInput.cast(inputs.itemById('platonicSolid'))
        platonicSolid       = platonicSolidInput.selectedItem.name

        triMethodInput      = adsk.core.DropDownCommandInput.cast(inputs.itemById('triangulationMethod'))
        triangulationMethod = triMethodInput.selectedItem.name

        secondaryTriMI      = adsk.core.DropDownCommandInput.cast(inputs.itemById('secondaryTriangulationMethod'))
        secondaryTriMethod  = secondaryTriMI.selectedItem.name

        frequencyInput      = adsk.core.IntegerSpinnerCommandInput.cast(inputs.itemById('frequencyInput'))
        frequency           = frequencyInput.value

        radiusInput         = adsk.core.ValueCommandInput.cast(inputs.itemById('radiusInput'))
        radius              = radiusInput.value

        xValInput           = adsk.core.ValueCommandInput.cast(inputs.itemById('xValInput'))
        xVal                = xValInput.value

        yValInput           = adsk.core.ValueCommandInput.cast(inputs.itemById('yValInput'))
        yVal                = yValInput.value

        zValInput           = adsk.core.ValueCommandInput.cast(inputs.itemById('zValInput'))
        zVal                = zValInput.value

        sphereInfo = adsk.core.TextBoxCommandInput.cast(inputs.itemById('sphereInfo'))

        root = _design.rootComponent

        # Draw the geodesic sphere
        geodesic = Geodesic(root, frequency, radius, xVal, yVal, zVal, platonicSolid, triangulationMethod, secondaryTriMethod)
        geodesic.render()

        sphereInfo.text = geodesic.info()

    except:
        _ui.messageBox('Unexpected failure:\n{}'.format(traceback.format_exc()))


def run(context):
    try:
        # Get the CommandDefinitions collection.
        cmdDefs = _ui.commandDefinitions

        # Create a button command definition.
        buttonDef = cmdDefs.addButtonDefinition('eveGeodesicSphere',
                                                'Geodesic sphere',
                                                'Creates geodesic sphere',
                                                './Resources/GeodesicSpheres')

        # Connect to the command created event.
        commandCreated = CommandCreatedEventHandler()
        buttonDef.commandCreated.add(commandCreated)
        handlers.append(commandCreated)

        # Get the CONSTRUCTION panel.
        constructionPanel = _ui.allToolbarPanels.itemById('SolidCreatePanel')

        # Add the button just below the PIPE command.
        constructionPanel.controls.addCommand(buttonDef, 'PrimitivePipe', False)

    except:
        _ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))


def stop(context):
    try:
        buttonDef = _ui.commandDefinitions.itemById('eveGeodesicSphere')
        if buttonDef:
            buttonDef.deleteMe()

        # Get the CONSTRUCTION panel.
        constructionPanel = _ui.allToolbarPanels.itemById('SolidCreatePanel')
        buttonControl = constructionPanel.controls.itemById('eveGeodesicSphere')
        if buttonControl:
            buttonControl.deleteMe()
    except:
        _ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))


class Geodesic:
    """Creates a geodesic sphere"""

    # Where to write the debug log output to
    logPath = 'C:\\debug.log'

    def __init__(self, root, frequency, radius, x, y, z, platonicSolid, triangulationMethod, secondaryTriMethod):
        """Constructor to setup references and create the component everything
        will live in and run setup()"""

        # Set the root component
        self.root = root

        # Set the radius
        self.radius = radius

        # Set the base coordinates
        self.x = x
        self.y = y
        self.z = z

        # Set the frequency
        self.frequency = frequency

        # Set the base platonic solid
        self.platonicSolid = platonicSolid

        # Set the triangulation method
        self.triangulationMethod = triangulationMethod

        # Set the secondary triangulation method
        self.secondaryTriangulationMethod = secondaryTriMethod

        # Create a reference to sketches
        self.sketches = self.root.sketches

        # Create an index of all points so we don't create 2 points at the same spot
        self.pointsIndex = {}

        # Holds all triangle lines within a section with each line being keys by it's ID
        self.tLines = {}

        # Lines created during first round of triangulation
        self.virtualLines1 = {}

        # Lines created during second round of triangulation
        self.virtualLines2 = {}

        self.doingSecondRoundTriangulation = False

        self.drawnLines = {}

        self.virtualTriangles1 = {}
        self.virtualTriangles2 = {}

        # An index of tuples containing line ID, start point ID and end point ID
        self.linesIndex = []

        self.triangles2 = []

        # Triangles within one section
        self.triangles = {}

        # Stores how many of each strut length there are
        self.strutLengths = {}

        # Set origin 3d point
        self.originPoint3D = adsk.core.Point3D.create(0, 0, 0)

        # Create sketch to hold a principle polyhedral triangle (PPT) of the geodesic sphere
        self.pptSketch = root.sketches.add(self.root.xYConstructionPlane)
        self.pptSketch.name = str(frequency) + 'v Geodesic principle polyhedral triangle'

        # Create sketch to hold the platonic solid
        self.platonicSketch = root.sketches.add(self.root.xYConstructionPlane)
        self.platonicSketch.name = str(frequency) + ' ' + self.platonicSolid

        # Create an origin point on the icosahedron sketch
        self.platonicOriginPoint = self.platonicSketch.sketchPoints.add(self.originPoint3D)

        # Create dictionary to hold platonic solid points
        self.platonicPoints = {}

        # Create dictionary to hold construction lines
        self.constructionLines = {}

        # Set initial component count
        self.initialCompCount = root.occurrences.count

        # Create the component everything else will live in
        location = adsk.core.Matrix3D.create()
        self.finalOccurrence = self.root.occurrences.addNewComponent(location)
        component = self.finalOccurrence.component
        component.name = str(self.frequency) + 'v Geodesic sphere'

        # This will hold one full segment of the dome that can be rotated round the z axis to form a full sphere
        self.fullSegment = adsk.core.ObjectCollection.create()

        # Create an array to hold all occurrences that we create
        self.createdOccurrences = []

        # Set platonic solid class
        if   platonicSolid == 'Icosahedron':  self.platonicSolidClass = Icosahedron(self, adsk)
        elif platonicSolid == 'Dodecahedron': self.platonicSolidClass = Dodecahedron(self, adsk)
        elif platonicSolid == 'Octahedron':   self.platonicSolidClass = None
        elif platonicSolid == 'Cube':         self.platonicSolidClass = None
        elif platonicSolid == 'Tetrahedron':  self.platonicSolidClass = None

        # TODO: PPT Point Count Limit

    def render(self):
        """Actually draw the geodesic sphere"""

        platonicSolid = self.platonicSolidClass

        # Draw all the points of the platonic solid
        self.drawPlatonicSolid()

        platonicSolid.rotateFlat()

        # Draw some constructions lines from the origin to points of the platonic solid
        self.drawConstructionLines()

        # Get the corners of the PPT. If the base solid doesn't start with triangles make them now
        corner1, corner2, corner3 = platonicSolid.createPPT()

        # Draw the PPT
        self.drawPPT(corner1, corner2, corner3)

        self.setVirtualTriangles()

        # Using the line index work out all the triangles within the section
        # self.setPptTriangles()

        if self.secondaryTriangulation():
            # Regenerate the list of triangles if we did secondary triangulation
            self.setVirtualTriangles()
            #self.setPptTriangles()

        self.drawVirtualTriangles()

        # return

        # If the origin is not 0,0,0 move the platonic solid to the correct location
        self.moveSketch(self.platonicSketch)
        self.moveSketch(self.pptSketch)

        # Create patches on all the triangles in section 1
        self.createPatches()

        # Use the PPT to create a full sphere
        platonicSolid.createFullSegment()
        platonicSolid.createSphere()

        # Each section in now in a separate component. Move these all into the one main geodesic component.
        self.moveEverythingToFinalComponent()

    def drawPlatonicSolid(self):
        """Draw the platonic base solid"""

        points = self.platonicSolidClass.points()

        # Loop points and create them on the base sketch
        for (name, coordinates) in points.items():
            x = (coordinates[0] * self.platonicSolidClass.factor)
            y = (coordinates[1] * self.platonicSolidClass.factor)
            z = (coordinates[2] * self.platonicSolidClass.factor)
            point = adsk.core.Point3D.create(x, y, z)
            self.platonicPoints[name] = self.platonicSketch.sketchPoints.add(point)

            ######################
            # Code to find out the name of each point
            # location = adsk.core.Matrix3D.create()
            # occurrence = self.root.occurrences.addNewComponent(location)
            # component = occurrence.component
            # component.name = name
            #
            # sketch = component.sketches.add(self.root.xYConstructionPlane)
            # sketch.name = name
            #
            # originPoint = sketch.sketchPoints.add(self.originPoint3D)
            # sketchPoint = sketch.sketchPoints.add(point)
            #
            # sketch.sketchCurves.sketchLines.addByTwoPoints(originPoint, sketchPoint)
            ######################

    def moveSketch(self, sketch):
        """If the origin is not 0,0,0 move the sketch to the correct location"""

        if self.x == 0 and self.y == 0 and self.z == 0:
            return

        linesPoints = adsk.core.ObjectCollection.create()
        for c in sketch.sketchCurves:
            linesPoints.add(c)
        for p in sketch.sketchPoints:
            linesPoints.add(p)

        transform = adsk.core.Matrix3D.create()
        transform.translation = adsk.core.Vector3D.create(self.x, self.y, self.z)
        sketch.move(linesPoints, transform)

    def drawConstructionLines(self):
        """Create some constructions lines from the origin to all points
        of the platonic solid. Some of these will be used later as axis of rotation"""

        origin = self.platonicOriginPoint
        sketchLines = self.platonicSketch.sketchCurves.sketchLines

        for name, point in self.platonicPoints.items():
            line = sketchLines.addByTwoPoints(origin, self.platonicPoints[name])
            line.isConstruction = True
            self.constructionLines[name] = line

    def drawPPT(self, corner1, corner2, corner3):
        """Draw the principle polyhedral triangle by the chosen triangulation method"""

        icoPoint1 = self.platonicPoints[corner1]
        icoPoint2 = self.platonicPoints[corner2]
        icoPoint3 = self.platonicPoints[corner3]

        # Those points are on the icosahedron sketch, make new ones on the PPT sketch
        point1 = self.createPointFromObject(icoPoint1)
        point2 = self.createPointFromObject(icoPoint2)
        point3 = self.createPointFromObject(icoPoint3)

        if self.triangulationMethod == 'Class I':
            self.drawPPTClassI(point1, point2, point3)
        elif self.triangulationMethod == 'Class II':
            self.drawPPTClassII(point1, point2, point3)
        elif self.triangulationMethod == 'Centre point':
            self.drawPPTCentrePoint(point1, point2, point3)

    def secondaryTriangulation(self):
        """Secondary triangulation"""
        if self.secondaryTriangulationMethod == 'None':
            return False

        self.frequency = 2

        self.doingSecondRoundTriangulation = True

        for triangleID, (lineIDA, lineIDB, lineIDC) in self.virtualTriangles1.items():
            (point1A, point2A, point1IDA, point2IDA) = self.virtualLines1[lineIDA]
            (point1B, point2B, point1IDB, point2IDB) = self.virtualLines1[lineIDB]
            (point1C, point2C, point1IDC, point2IDC) = self.virtualLines1[lineIDC]

            point1 = point1A
            point2 = point2A

            point3 = None

            if point1IDB != point1IDA and point1IDB != point2IDA:
                point3 = point1B
            elif point2IDB != point1IDA and point2IDB != point2IDA:
                point3 = point2B
            elif point1IDC != point1IDA and point1IDC != point2IDA:
                point3 = point1C
            elif point2IDC != point1IDA and point2IDC != point2IDA:
                point3 = point2C

            if point3 is None:
                return

            if self.secondaryTriangulationMethod == 'Class I':
                self.drawPPTClassI(point1, point2, point3)
            elif self.secondaryTriangulationMethod == 'Class II':
                self.drawPPTClassII(point1, point2, point3)
            elif self.secondaryTriangulationMethod == 'Centre point':
                self.drawPPTCentrePoint(point1, point2, point3)



        # for name, lines in self.triangles.items():
        #
        #     point1, point2, point3 = self.getUniquePointsFromLines(lines)
        #
        #     if self.secondaryTriangulationMethod == 'Class I':
        #         self.drawPPTClassI(point1, point2, point3)
        #     elif self.secondaryTriangulationMethod == 'Class II':
        #         self.drawPPTClassII(point1, point2, point3)
        #     elif self.secondaryTriangulationMethod == 'Centre point':
        #         self.drawPPTCentrePoint(point1, point2, point3)

        return True

    def drawPPTClassI(self, point1, point2, point3):
        """Draw the principle polyhedral triangle.
        It would be great to have code that is not frequency specific but very hard to do."""

        # Create triangle sides - generated points are returned
        side1 = self.createTriangleCutThrough(point1, point2, self.frequency)
        side2 = self.createTriangleCutThrough(point2, point3, self.frequency)
        side3 = self.createTriangleCutThrough(point3, point1, self.frequency)

        if self.frequency == 1:
            # When frequency is 1 no more sub division is required
            return

        # Creates lines connecting sides
        points1 = self.connectSides(side1, side2)
        points2 = self.connectSides(side2, side3)
        points3 = self.connectSides(side3, side1)

        if self.frequency == 2:
            return

        if self.frequency == 3:

            # Draw lines to centre and we're done
            points = points1[0] + points2[0] + points3[0]
            centrePoint = self.getExtendedCentre([point1, point2, point3])
            self.drawLinesToCentre(points, centrePoint)
            return

        # Use centre point of new line to draw triangle back towards corner
        self.createLine(points1[1][1], points1[0][0])
        self.createLine(points1[1][1], points1[0][1])

        self.createLine(points2[1][1], points2[0][0])
        self.createLine(points2[1][1], points2[0][1])

        self.createLine(points3[1][1], points3[0][0])
        self.createLine(points3[1][1], points3[0][1])

        if self.frequency == 4 or self.frequency == 5:

            # Use all 3 centre points to create centre triangle
            subdivisions = 2 if self.frequency == 5 else 1
            subPoints1 = self.createTriangleCutThrough(points3[1][1], points1[1][1], subdivisions)
            subPoints2 = self.createTriangleCutThrough(points1[1][1], points2[1][1], subdivisions)
            subPoints3 = self.createTriangleCutThrough(points2[1][1], points3[1][1], subdivisions)

            if self.frequency == 5:

                # Create lines to edges and connect 3 centre points
                self.createLine(subPoints1[1], side1[2])
                self.createLine(subPoints1[1], side1[3])

                self.createLine(subPoints2[1], side2[2])
                self.createLine(subPoints2[1], side2[3])

                self.createLine(subPoints3[1], side3[2])
                self.createLine(subPoints3[1], side3[3])

                self.createLine(subPoints1[1], subPoints2[1])
                self.createLine(subPoints2[1], subPoints3[1])
                self.createLine(subPoints3[1], subPoints1[1])

        if self.frequency == 6:

            # Use centre point of new line to draw triangle using next most inner line
            pointToCentre = []
            for points in [points1, points2, points3]:
                self.createLine(points[1][1], points[2][1])
                self.createLine(points[1][1], points[2][2])
                self.createLine(points[1][0], points[2][1])
                self.createLine(points[1][2], points[2][2])
                pointToCentre.extend([points[2][1], points[2][2]])

            # Connect corners of inner most lines
            self.createLine(points1[2][1], points3[2][2])
            self.createLine(points3[2][1], points2[2][2])
            self.createLine(points2[2][1], points1[2][2])

            # Draw lines to centre and we're done
            # Find the centre of the triangle pushed to the outer sphere
            centrePoint = self.getExtendedCentre([point1, point2, point3])
            self.drawLinesToCentre(pointToCentre, centrePoint)

    def connectSides(self, side1Points, side2Points):
        """Draw connecting lines from one side of the triangle to another. Things get a little funky in here."""

        # The number of points on one side affects how many lines with how many sub divisions there are
        countPoints = len(side1Points)

        # Figure out how many connecting lines are needed
        linesToDraw = countPoints - 3
        if countPoints == 6:
            linesToDraw = 2
        if countPoints == 7:
            linesToDraw = 3

        # This defines which points should connect to each other for each specific points count at each diff.
        # There is probably a more elegant way to do this.
        subDivCounts = {
            4: {1: 1},
            5: {0: 2, 2: 1},
            6: {1: 2, 3: 1},
            7: {0: 3, 2: 2, 4: 1},
        }

        # Create an array to return any points created
        points = []

        # i is counting backwards from the number of points on one side
        for i in reversed(range(1, countPoints)):

            # i2 is counting forwards
            i2 = countPoints - i - 1

            # Never draw lines to the corners
            if i == 0 or i2 == 0:
                continue

            # Find the difference between the two counts
            diff = i - i2

            # Get the subdivision count from the subDivCounts dictionary
            subdivisions = 1
            if countPoints in subDivCounts:
                if diff in subDivCounts[countPoints]:
                    subdivisions = subDivCounts[countPoints][diff]

            # Finally create a cut through with the correct number of sub divisions
            point1 = side1Points[i]
            point2 = side2Points[i2]
            points.append(self.createTriangleCutThrough(point1, point2, subdivisions))

            linesToDraw -= 1
            if linesToDraw == 0:
                return points

        return points

    def drawLinesToCentre(self, points, centrePoint):
        """Draw a line from all the points passed in to the extended centre of the triangle"""

        for point in points:
            self.createLine(centrePoint, point)

    def createTriangleCutThrough(self, point1, point2, subdivisions):
        """Create all the lines that make up one cut-through of one of the icosahedron
        triangles. Each point is pushed to the outside of the sphere. To do
        this we need to make a plane through these three points and a sketch
        so when we draw an arc it knows which direction to go in."""

        if subdivisions == 1:

            # No need to create a plane or sketch - just draw a line between the two points
            self.createLine(point1, point2)
            return [point1, point2]

        # Create plane through 2 points and origin and sketch
        plane, sketch = self.createSketchOnPlane(point1, point2)

        # Find where points in 3d space intersect with our new plane
        entities = [point1, point2]
        intersectPos = sketch.intersectWithSketchPlane(entities)

        # Draw arcs to find locations for new points
        sketchArcs = sketch.sketchCurves.sketchArcs
        centre = self.originPoint3D
        start = intersectPos[0]
        arcs = []

        # Find angle between the two points
        angleBetweenPoints = self.getAngleBetweenTwoPoints(point1, point2)
        sectionAngle = angleBetweenPoints / subdivisions

        for i in range(1, subdivisions):
            angle = sectionAngle * i
            arc = sketchArcs.addByCenterStartSweep(centre, start, angle)
            arcs.append(arc)

        # Create all the points along the edge
        points = []
        limit = subdivisions + 1
        for i in range(0, limit):
            # If first or last item use the icosahedron point position
            if i == 0 or i == limit - 1:
                index = 0 if (i == 0) else 1
                point = self.create3dPointFromObject(intersectPos[index])
            # Else use the arc end points for the intermediary points
            else:
                index = i - 1
                point = self.create3dPointFromObject(arcs[index].endSketchPoint)

            points.append(point)

        # Initialize an array to hold the all the points on this side
        sidePoints = []

        # Draw the lines
        for i in range(0, subdivisions):
            line = self.createLine(points[i], points[i + 1])
            point = self.createPointFromObject(line.startSketchPoint)
            sidePoints.append(point)

            if i == subdivisions - 1:
                # And add the final point to the side points array
                point = self.createPointFromObject(line.endSketchPoint)
                sidePoints.append(point)

        # Clean up unneeded lines, planes and sketches
        for arc in arcs:
            arc.deleteMe()

        plane.deleteMe()
        sketch.deleteMe()

        return sidePoints

    def createSketchOnPlane(self, point1, point2):
        """Create a plane through the 2 points passed in and the origin and
        create a sketch on that plane"""

        # Create construction plane input
        planeInput = self.root.constructionPlanes.createInput()

        # Third point is always the origin
        point3 = self.root.originConstructionPoint

        # Add construction plane by three points
        planeInput.setByThreePoints(point3, point1, point2)
        plane = self.root.constructionPlanes.add(planeInput)

        # Add sketch on new plane
        sketch = self.root.sketches.add(plane)

        return plane, sketch

    def drawPPTClassII(self, point1, point2, point3):

        if self.frequency == 1:
            self.createTriangleCutThrough(point1, point2, 1)
            self.createTriangleCutThrough(point2, point3, 1)
            self.createTriangleCutThrough(point3, point1, 1)
            return

        # Doing any more than this will just be so slow it's not worth it
        # 4 iterations took my 2.8Ghz i7 about half an hour and that's not a good UX
        iterations = self.frequency if self.frequency <= 3 else 3

        self.classIITriangulate(point1, point2, point3, iterations)

    def classIITriangulate(self, point1, point2, point3, iterations):

        iterations -= 1

        if iterations <= 0:
            return

        centrePoint = self.getExtendedCentre([point1, point2, point3])

        # Connect them up
        side1 = self.createTriangleCutThrough(point1, point2, 2)
        side2 = self.createTriangleCutThrough(point2, point3, 2)
        side3 = self.createTriangleCutThrough(point3, point1, 2)

        # And connect to centre
        points = [point1, point2, point3, side1[1], side2[1], side3[1]]
        self.drawLinesToCentre(points, centrePoint)

        # Return an array of new triangle points created
        new_points = [
            (side1[0], side1[1], centrePoint),
            (side1[1], side1[2], centrePoint),
            (side2[0], side2[1], centrePoint),
            (side2[1], side2[2], centrePoint),
            (side3[0], side3[1], centrePoint),
            (side3[1], side3[2], centrePoint),
        ]

        for point1, point2, point3 in new_points:
            self.classIITriangulate(point1, point2, point3, iterations)

    def drawPPTCentrePoint(self, point1, point2, point3):
        """Triangulate with the centre point method"""

        self.createTriangleCutThrough(point1, point2, 1)
        self.createTriangleCutThrough(point2, point3, 1)
        self.createTriangleCutThrough(point3, point1, 1)

        if self.frequency == 1:
            return

        #
        iterations = self.frequency if self.frequency <= 4 else 4

        self.centrePointTriangulate([point1, point2, point3], iterations)

    def centrePointTriangulate(self, points, iterations):

        iterations -= 1

        if iterations <= 0:
            return

        centrePoint = self.getExtendedCentre(points)
        self.drawLinesToCentre(points, centrePoint)

        new_points = [
            (points[0], points[1], centrePoint),
            (points[1], points[2], centrePoint),
            (points[2], points[0], centrePoint),
        ]

        for points in new_points:
            self.centrePointTriangulate(points, iterations)


    def createPointFromObject(self, obj):
        """Create and return a new sketch point from the world geometry of the object passed in"""

        pointID = self.pointID(obj.worldGeometry)

        if pointID not in self.pointsIndex:
            point3D = self.create3dPointFromObject(obj)
            self.pointsIndex[pointID] = self.pptSketch.sketchPoints.add(point3D)

        return self.pointsIndex[pointID]

    def createLine(self, point1, point2):
        """Create one line of the geodesic and store a named reference to it in the tLines dictionary"""

        if point1.objectType == 'adsk::core::Point3D':
            point1 = self.pptSketch.sketchPoints.add(point1)

        if point2.objectType == 'adsk::core::Point3D':
            point2 = self.pptSketch.sketchPoints.add(point2)

        point1ID = Geodesic.pointID(point1.worldGeometry)
        point2ID = Geodesic.pointID(point2.worldGeometry)

        lineIDs = sorted([point1ID, point2ID])
        lineID = str(lineIDs[0]) + ';' + str(lineIDs[1])

        if self.doingSecondRoundTriangulation:
            self.virtualLines2[lineID] = (point1, point2, point1ID, point2ID)
        else:
            self.virtualLines1[lineID] = (point1, point2, point1ID, point2ID)

        # for lineID, (point1, point2, point1ID, point2ID) in self.virtualLines.items():
        # virtualTriangles

        return fakeLine(point1, point2)

        sketchLines = self.pptSketch.sketchCurves.sketchLines
        line = sketchLines.addByTwoPoints(point1, point2)
        name = self.lineID(line)
        self.tLines[name] = line

        # Add all lines created to the line index
        lineEndsIDs = self.lineEndsIDs(line)
        self.linesIndex.append([name, lineEndsIDs[0], lineEndsIDs[1]])

        # Keep a count of how many of each strut lengths are used
        length = round(line.length, 2)
        if length in self.strutLengths:
            self.strutLengths[length] += 1
        else:
            self.strutLengths[length] = 1

        return line

    def setVirtualTriangles(self):

        if self.doingSecondRoundTriangulation:
            virtualLines = self.virtualLines2
            virtualTriangles = self.virtualTriangles2
        else:
            virtualLines = self.virtualLines1
            virtualTriangles = self.virtualTriangles1

        for lineIDA, (point1A, point2A, point1IDA, point2IDA) in virtualLines.items():
            for lineIDB, (point1B, point2B, point1IDB, point2IDB) in virtualLines.items():

                lineIDC = None

                if point1IDA == point1IDB:
                    lineIDC = self.getThirdVirtualLine(point2IDA, point2IDB, virtualLines)
                elif point2IDA == point2IDB:
                    lineIDC = self.getThirdVirtualLine(point1IDA, point1IDB, virtualLines)
                elif point1IDA == point2IDB:
                    lineIDC = self.getThirdVirtualLine(point2IDA, point1IDB, virtualLines)
                elif point2IDA == point1IDB:
                    lineIDC = self.getThirdVirtualLine(point1IDA, point2IDB, virtualLines)

                if lineIDC is not None:
                    lineIDs = sorted([lineIDA, lineIDB, lineIDC])
                    triangleID = lineIDs[0] + '+' + lineIDs[1] + '+' + lineIDs[2]
                    virtualTriangles[triangleID] = (lineIDA, lineIDB, lineIDC)

    def drawVirtualTriangles(self):

        if self.doingSecondRoundTriangulation:
            virtualLines = self.virtualLines2
            virtualTriangles = self.virtualTriangles2
        else:
            virtualLines = self.virtualLines1
            virtualTriangles = self.virtualTriangles1

        for triangleID, (lineIDA, lineIDB, lineIDC) in virtualTriangles.items():
            (point1A, point2A, point1IDA, point2IDA) = virtualLines[lineIDA]
            (point1B, point2B, point1IDB, point2IDB) = virtualLines[lineIDB]
            (point1C, point2C, point1IDC, point2IDC) = virtualLines[lineIDC]

            sketchLines = self.pptSketch.sketchCurves.sketchLines

            if lineIDA not in self.drawnLines:
                line1 = sketchLines.addByTwoPoints(point1A, point2A)
                self.drawnLines[lineIDA] = line1
            else:
                line1 = self.drawnLines[lineIDA]

            if lineIDB not in self.drawnLines:
                line2 = sketchLines.addByTwoPoints(point1B, point2B)
                self.drawnLines[lineIDB] = line2
            else:
                line2 = self.drawnLines[lineIDB]

            if lineIDC not in self.drawnLines:
                line3 = sketchLines.addByTwoPoints(point1C, point2C)
                self.drawnLines[lineIDC] = line3
            else:
                line3 = self.drawnLines[lineIDC]

            # Add all lines in the triangle to a collection
            lines = adsk.core.ObjectCollection.create()
            lines.add(line1)
            lines.add(line2)
            lines.add(line3)

            # And add the triangle to the triangles index
            triangleID = self.triangleID(lines)

            if triangleID not in self.triangles:
                self.triangles[triangleID] = lines

    def getThirdVirtualLine(self, targetPoint1ID, targetPoint2ID, virtualLines):
        for lineID, (point1, point2, point1ID, point2ID) in virtualLines.items():
            if (targetPoint1ID == point1ID and targetPoint2ID == point2ID  or
                    targetPoint1ID == point2ID and targetPoint2ID == point1ID):
                return lineID

    def getExtendedCentre(self, points):
        """Find the centre of the 3 points passed in, draw a line from the
        origin through that point the length of the radius and then return a
        point at that location"""

        # Find the centre of the triangle
        centrePoint = self.getCentrePoint(points)

        # Create a vector through the triangle centre and extend it's length to the radius
        wg = centrePoint.worldGeometry
        vector = adsk.core.Vector3D.create(wg.x, wg.y, wg.z)
        vector.normalize()
        vector.scaleBy(self.radius)

        # Now we can delete the triangle centre point
        centrePoint.deleteMe()

        # Create a point at the end of the vector
        point3D = adsk.core.Point3D.create(vector.x, vector.y, vector.z)
        return self.pptSketch.sketchPoints.add(point3D)

    def getCentrePoint(self, points):
        """Find the centre of the 3 points passed in and return a sketchPoint
        located there"""

        ax = ay = az = 0
        for point in points:
            ax += point.worldGeometry.x
            ay += point.worldGeometry.y
            az += point.worldGeometry.z

        point3D = adsk.core.Point3D.create(ax / 3, ay / 3, az / 3)
        return self.pptSketch.sketchPoints.add(point3D)

    def setPptTriangles(self):
        """Loop through all the lines to find out which ones make triangles"""

        # Loop lines
        for item1 in self.linesIndex:
            nameL1 = item1[0]
            startL1 = item1[1]
            endL1 = item1[2]

            # Build an array of every line that touches this line
            touchingLines = []
            touchingLines.extend(self.linesWithThisPoint(startL1, nameL1))
            touchingLines.extend(self.linesWithThisPoint(endL1, nameL1))

            # Loop touching lines to find the third line of the triangle
            for touchingLine in touchingLines:
                nameL2 = touchingLine[0]
                startL2 = touchingLine[1]
                endL2 = touchingLine[2]

                point1 = point2 = None

                # Find the two points that are not touching
                if startL2 == startL1:
                    point1 = endL1
                    point2 = endL2
                elif startL2 == endL1:
                    point1 = startL1
                    point2 = endL2
                elif endL2 == startL1:
                    point1 = endL1
                    point2 = startL2
                elif endL2 == endL1:
                    point1 = startL1
                    point2 = startL2

                if point1 is None or point2 is None:
                    continue

                # Now find the line that connects the two points
                nameL3 = self.lineWithThesePoints(point1, point2, nameL1, nameL2)

                if nameL3 is not None:

                    # Add all lines in the triangle to a collection
                    lines = adsk.core.ObjectCollection.create()
                    lines.add(self.tLines[nameL1])
                    lines.add(self.tLines[nameL2])
                    lines.add(self.tLines[nameL3])

                    # And add the triangle to the triangles index
                    triangleID = self.triangleID(lines)

                    if triangleID not in self.triangles:
                        self.triangles[triangleID] = lines

    def lineWithThesePoints(self, point1, point2, excludeLine1, excludeLine2):
        """Find a line that connects the two points passed in"""

        for line in self.linesIndex:
            name = line[0]
            startPointID = line[1]
            endPointID = line[2]

            if name == excludeLine1 or name == excludeLine2:
                continue

            normalMatch = startPointID == point1 and endPointID == point2
            reverseMatch = startPointID == point2 and endPointID == point1

            if normalMatch or reverseMatch:
                return name

        return None

    def linesWithThisPoint(self, pointID, excludeLineID):
        """Returns an array of lines where one end is on the point passed in"""

        lines = []

        for line in self.linesIndex:
            name = line[0]
            startPointID = line[1]
            endPointID = line[2]

            if name == excludeLineID:
                continue

            if startPointID == pointID or endPointID == pointID:
                lines.append(line)

        return lines

    def createPatches(self):
        """Create a patch for each triangle in the geodesic segment. Put all
        patches in a new child component"""

        location = adsk.core.Matrix3D.create()
        occurrence = self.root.occurrences.addNewComponent(location)
        comp = occurrence.component
        comp.name = 'Segment'

        for name, lines in self.triangles.items():
            self.createPatchFrom3Lines(lines, comp)

    def rotateTriangleSegment(self, occurrence, axis, quantity, angle):
        """Rotate the component around the axis by the angle and create a copy.
        Returns the new component."""

        # Create input entities for circular pattern
        inputEntities = adsk.core.ObjectCollection.create()
        inputEntities.add(occurrence)

        # Create the input for circular pattern
        circularFeats = self.root.features.circularPatternFeatures
        circularInput = circularFeats.createInput(inputEntities, axis)

        # Set the quantity of the elements
        circularInput.quantity = adsk.core.ValueInput.createByReal(quantity)

        # Set the angle of the circular pattern
        circularInput.totalAngle = adsk.core.ValueInput.createByString(angle)

        # Set symmetry of the circular pattern
        circularInput.isSymmetric = False

        # Create the circular pattern
        return circularFeats.add(circularInput)

    def setNewFullSegment(self, numItems):
        """Reset the full segment to the last x items created"""

        self.fullSegment = adsk.core.ObjectCollection.create()
        for occurrence in self.root.occurrences[-numItems:]:
            self.fullSegment.add(occurrence)

    def mirrorInPlanes(self, mirrorPlanes):
        """Mirror the full segment in all the mirror planes passed in"""

        for letter1, letter2 in mirrorPlanes:

            # Create construction plane input
            planeInput = self.root.constructionPlanes.createInput()

            point1 = self.platonicPoints[letter1]
            point2 = self.platonicPoints[letter2]
            point3 = self.root.originConstructionPoint

            # Add construction plane by three points
            planeInput.setByThreePoints(point3, point1, point2)
            plane = self.root.constructionPlanes.add(planeInput)

            mirrorFeatures = self.root.features.mirrorFeatures
            mirrorInput = mirrorFeatures.createInput(self.fullSegment, plane)

            try:
                # Create the mirror feature
                mirrorFeatures.add(mirrorInput)
            except:
                # Fusion is raising a warning here but the operation works fine so lets just ignore it
                pass

            plane.deleteMe()

    def moveEverythingToFinalComponent(self):
        """Move all the components we've made into the final geodesic component"""

        for o in self.createdOccurrences:
            o.moveToComponent(self.finalOccurrence)

    def info(self):
        """Print out info about this sphere"""

        numSections = len(self.createdOccurrences)

        numLines = len(self.tLines) * numSections
        numTriangles = len(self.triangles) * numSections

        info  = 'Number of lines: ' + str(numLines) + '\n'
        info += 'Number of triangles: ' + str(numTriangles) + '\n'

        letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
                   'U', 'V', 'W', 'X', 'Y', 'Z']

        for length in sorted(self.strutLengths):
            count = self.strutLengths[length] * numSections
            letter = letters.pop(0)
            info += 'Strut {} ({}cm) x {}'.format(letter, length, count) + '\n'

        # info += 'Platonic Solid: ' + self.platonicSolid + '\n'
        # info += 'Triangulation Method: ' + self.triangulationMethod + '\n'

        return info

    @staticmethod
    def log(msg):
        pass
        """Write to a log file"""
        # with open(Geodesic.logPath, 'a') as f:
        #     f.write(str(msg) + '\n')

    @staticmethod
    def clearLog():
        pass
        """Clear the log file"""
        # open(Geodesic.logPath, 'w').close()

    @staticmethod
    def getUniquePointsFromLines(lines):
        """Get the unique points from the object collection of lines passed in"""

        line1 = lines.item(0)
        line2 = lines.item(1)
        line3 = lines.item(2)

        points = [
            line1.startSketchPoint,
            line1.endSketchPoint,
            line2.startSketchPoint,
            line2.endSketchPoint,
            line3.startSketchPoint,
            line3.endSketchPoint,
        ]

        uniquePoints = {}
        for point in points:
            wg = point.worldGeometry
            pointID = str(wg.x) + str(wg.y) + str(wg.z)
            uniquePoints[pointID] = point

        points = []
        for key, point in uniquePoints.items():
            points.append(point)

        point1 = points[0]
        point2 = points[1]
        point3 = points[2]

        return point1, point2, point3

    @staticmethod
    def triangleID(lines):
        """Create a unique identifier for a triangle"""
        coordinates = []
        coordinates.extend(Geodesic.lineEndsIDs(lines.item(0)))
        coordinates.extend(Geodesic.lineEndsIDs(lines.item(1)))
        coordinates.extend(Geodesic.lineEndsIDs(lines.item(2)))

        # Make list unique and sort
        unique = list(set(coordinates))
        unique.sort()

        return ''.join(unique)

    @staticmethod
    def lineID(line):
        """Returns a single string that uniquely this line"""
        coordIdents = Geodesic.lineEndsIDs(line)
        return coordIdents[0] + ' x ' + coordIdents[1]

    @staticmethod
    def lineEndsIDs(line):
        """Returns an array of 2 strings that uniquely identifies a line"""
        coordinates = [
            Geodesic.pointID(line.worldGeometry.startPoint),
            Geodesic.pointID(line.worldGeometry.endPoint),
        ]
        return sorted(coordinates)

    @staticmethod
    def create3dPointFromObject(obj):
        """Returns a 3D point created from the world geometry of the object passed in"""
        wg = obj.worldGeometry
        return adsk.core.Point3D.create(wg.x, wg.y, wg.z)

    @staticmethod
    def pointID(Point3D):
        """Return a unique ID for this point"""
        dps = 2
        x = str(round(Point3D.x, dps)).replace('-0.0', '0.0')
        y = str(round(Point3D.y, dps)).replace('-0.0', '0.0')
        z = str(round(Point3D.y, dps)).replace('-0.0', '0.0')
        return '{},{},{}'.format(x, y, z)

    @staticmethod
    def createPatchFrom3Lines(lines, comp):
        """Create and return a patch based off the 3 line names passed in"""
        patches = comp.features.patchFeatures
        newBodyOp = adsk.fusion.FeatureOperations.NewBodyFeatureOperation
        patchInput = patches.createInput(lines, newBodyOp)
        return patches.add(patchInput)

    @staticmethod
    def getAngleBetweenTwoPoints(point1, point2):
        """Find and return the angle between the two points passed in"""
        wg1 = point1.worldGeometry
        vector1 = adsk.core.Vector3D.create(wg1.x, wg1.y, wg1.z)
        wg2 = point2.worldGeometry
        vector2 = adsk.core.Vector3D.create(wg2.x, wg2.y, wg2.z)
        return vector1.angleTo(vector2)

class fakeLine:

    def __init__(self, point1, point2):
        self.startSketchPoint = point1
        self.endSketchPoint = point2
