from abc import ABC, abstractmethod

class AbstractPlatonicSolid(ABC):

    def __init__(self, radius):
        self.radius = radius
        super().__init__()

    @abstractmethod
    def draw(self):
        pass

    @abstractmethod
    def rotate(self):
        pass