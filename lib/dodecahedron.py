import math
from .platonicSolid import PlatonicSolid

class Dodecahedron(PlatonicSolid):

    def __init__(self, Geodesic, adsk):
        super().__init__(Geodesic, adsk)
        self.factor = 1

    def points(self):
        """Return an array of points that define an icosahedron"""

        a = self.Geodesic.radius / math.sqrt(3)
        b = self.Geodesic.radius / (math.sqrt(3) * self.GR)
        c = (self.Geodesic.radius * self.GR) / math.sqrt(3)

        points = {
            'A': [a, a, a], 'B': [-a, a, a], 'C': [a, -a, a], 'D': [a, a, -a],
            'E': [-a, -a, a], 'F': [-a, a, -a], 'G': [a, -a, -a], 'H': [-a, -a, -a],
            'I': [0, b, c], 'J': [0, -b, c], 'K': [0, b, -c], 'L': [0, -b, -c],
            'M': [b, c, 0], 'N': [-b, c, 0], 'O': [b, -c, 0], 'P': [-b, -c, 0],
            'Q': [c, 0, b], 'R': [-c, 0, b], 'S': [c, 0, -b], 'T': [-c, 0, -b],
        }

        return points

    def rotateFlat(self):
        pass

    def createPPT(self):
        Geodesic = self.Geodesic
        adsk = self.adsk

        wg1 = Geodesic.platonicPoints['P'].worldGeometry
        wg2 = Geodesic.platonicPoints['E'].worldGeometry
        wg3 = Geodesic.platonicPoints['H'].worldGeometry
        wg4 = Geodesic.platonicPoints['R'].worldGeometry
        wg5 = Geodesic.platonicPoints['T'].worldGeometry

        xa = (wg1.x + wg2.x + wg3.x + wg4.x + wg5.x) / 5
        ya = (wg1.y + wg2.y + wg3.y + wg4.y + wg5.y) / 5
        za = (wg1.z + wg2.z + wg3.z + wg4.z + wg5.z) / 5

        vector = adsk.core.Vector3D.create(xa, ya, za)
        vector.normalize()
        vector.scaleBy(Geodesic.radius)

        # Create a point at the end of the vector
        point3D = adsk.core.Point3D.create(vector.x, vector.y, vector.z)
        Geodesic.platonicPoints['U'] = Geodesic.platonicSketch.sketchPoints.add(point3D)

        # Add construction line to new point
        origin = Geodesic.platonicOriginPoint
        sketchLines = Geodesic.platonicSketch.sketchCurves.sketchLines
        line = sketchLines.addByTwoPoints(origin, Geodesic.platonicPoints['U'])
        line.isConstruction = True
        Geodesic.constructionLines['U'] = line

        return 'P', 'E', 'U'

    def createFullSegment(self):
        Geodesic = self.Geodesic

        axis = Geodesic.constructionLines['U']

        count = len(Geodesic.root.occurrences)

        section = Geodesic.root.occurrences.item(count - 1)
        Geodesic.rotateTriangleSegment(section, axis, 5, '(360 / 5) * 4')

        for i in range(1, 6):
            section = Geodesic.root.occurrences.item(count + i - 2)
            Geodesic.fullSegment.add(section)

    def createSphere(self):
        Geodesic = self.Geodesic

        # Planes to mirror the initial full segment in
        mirrorPlanes = [
            ('H', 'P'),
            ('P', 'E'),
            ('E', 'R'),
            ('R', 'T'),
            ('T', 'H'),
        ]
        Geodesic.mirrorInPlanes(mirrorPlanes)

        # New full segment and mirror planes
        Geodesic.setNewFullSegment(5)
        mirrorPlanes = [('L', 'K'), ('K', 'F')]
        Geodesic.mirrorInPlanes(mirrorPlanes)

        # New full segment and mirror planes
        Geodesic.setNewFullSegment(5)
        mirrorPlanes = [('D', 'M'), ('M', 'N')]
        Geodesic.mirrorInPlanes(mirrorPlanes)

        # New full segment and mirror planes
        Geodesic.setNewFullSegment(5)
        mirrorPlanes = [('I', 'A')]
        Geodesic.mirrorInPlanes(mirrorPlanes)

        # New full segment and mirror planes
        Geodesic.setNewFullSegment(5)
        mirrorPlanes = [('C', 'Q')]
        Geodesic.mirrorInPlanes(mirrorPlanes)

        # Give all the created segments a sensible name and add them add to the createdOccurrences array
        i = 1
        for occurrence in Geodesic.root.occurrences[-60:]:
            occurrence.component.name = 'Segment ' + str(i)
            Geodesic.createdOccurrences.append(occurrence)
            i += 1
