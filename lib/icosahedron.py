import math
from .platonicSolid import PlatonicSolid

class Icosahedron(PlatonicSolid):

    # The angle to rotate everything to get a flat base
    OFFSET_ANGLE = 58.28252558854

    def __init__(self, Geodesic, adsk):
        super().__init__(Geodesic, adsk)
        self.factor = self.Geodesic.radius / (math.sqrt(1 + math.pow(self.GR, 2)))

    def points(self):
        """Return an array of points that define an icosahedron"""

        r = self.GR
        points = {
            'A': [0, 1, r], 'B': [0, -1, r], 'C': [0, -1, -r],
            'D': [0, 1, -r], 'E': [r, 0, 1], 'F': [-r, 0, 1],
            'G': [-r, 0, -1], 'H': [r, 0, -1], 'I': [1, r, 0],
            'J': [-1, r, 0], 'K': [-1, -r, 0], 'L': [1, -r, 0],
        }

        return points

    def rotateFlat(self):
        """The icosahedron is initially created on a bit of an angle. This function rotates it so the base on
                the dome would be flat against the xy plane."""

        Geodesic = self.Geodesic
        adsk = self.adsk

        linesPoints = adsk.core.ObjectCollection.create()
        for c in Geodesic.platonicSketch.sketchCurves:
            linesPoints.add(c)
        for p in Geodesic.platonicSketch.sketchPoints:
            linesPoints.add(p)

        vector1 = adsk.core.Vector3D.create(0, 1, 0)
        vector2 = adsk.core.Vector3D.create(0, 0, 1)

        axis = vector1.crossProduct(vector2)
        axis.transformBy(Geodesic.platonicSketch.transform)

        origin = Geodesic.platonicSketch.origin
        origin.transformBy(Geodesic.platonicSketch.transform)

        angle = (self.OFFSET_ANGLE + 90) * (math.pi / 180)

        matrix = adsk.core.Matrix3D.create()
        matrix.setToRotation(angle, axis, origin)

        Geodesic.platonicSketch.move(linesPoints, matrix)

    def createPPT(self):
        return 'C', 'D', 'H'

    def createFullSegment(self):
        """Copy the first patch section so it creates a full segment
        that can be rotated round the z axis to create a full sphere.
        Returns a collection with all sections (1 full segment)"""

        Geodesic = self.Geodesic

        # Define the occurrences and axis they should be rotated round
        rotations = [(1, 'H'), (2, 'L'), (3, 'L'), (4, None)]

        for i, axis in rotations:
            section = Geodesic.root.occurrences.item(i)

            if axis is not None:
                axis = Geodesic.constructionLines[axis]
                Geodesic.rotateTriangleSegment(section, axis, 2, '(360 / 5) * 4')

            Geodesic.createdOccurrences.append(section)
            Geodesic.fullSegment.add(section)

    def createSphere(self):
        """Rotate the segment passed in round the z axis to create a full sphere"""

        Geodesic = self.Geodesic
        adsk = self.adsk

        # Create the input for circular pattern
        circularFeats = Geodesic.root.features.circularPatternFeatures
        circularFeatInput = circularFeats.createInput(Geodesic.fullSegment, Geodesic.constructionLines['D'])

        # Set the quantity of the elements
        circularFeatInput.quantity = adsk.core.ValueInput.createByReal(5)

        # Set the angle of the circular pattern
        circularFeatInput.totalAngle = adsk.core.ValueInput.createByString('360 deg')

        # Set symmetry of the circular pattern
        circularFeatInput.isSymmetric = False

        # Create the circular pattern
        circularFeats.add(circularFeatInput)

        # Add each section just created to the created occurrences array
        rangeFrom = Geodesic.initialCompCount + 5
        rangeTo = Geodesic.initialCompCount + 21
        for x in range(rangeFrom, rangeTo):
            Geodesic.createdOccurrences.append(Geodesic.root.occurrences.item(x))



