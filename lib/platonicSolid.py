import math

class PlatonicSolid:

    def __init__(self, Geodesic, adsk):
        self.Geodesic = Geodesic
        self.adsk = adsk
        self.GR = (1 + math.sqrt(5)) / 2
